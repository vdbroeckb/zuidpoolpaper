n = 1000000;
afstand = 100;
stap = afstand/n;

hoek = -45;
rotate = [cosd(hoek) -sind(hoek); sind(hoek) cosd(hoek)];

x = zeros(n+1,1);
y = zeros(n+1,1);
y(2) = stap;
for i = 3:n+1
    v = [x(i-1) y(i-1)];
    dist = sqrt(v(1)^2 + v(2)^2);
    absv = v/dist;
    nextstep = absv * rotate * stap;
    x(i) = x(i-1) + nextstep(1);
    y(i) = y(i-1) + nextstep(2);
end

totaleAfstand = sqrt(x(end)^2+y(end)^2);


fprintf("wanneer men %d km loopt vanaf de zuidpool \n en steeds %d graden van het noorden wandelt, \n eindigt men %5f km van de zuidpool\n", afstand, hoek, totaleAfstand);
stop

f = figure;

plot(x,y,'-k')

THETA=linspace(0,2*pi,100);
RHO=ones(1,100)*100;
[X,Y] = pol2cart(THETA,RHO);
RHO=ones(1,100)*75;
[X1,Y1] = pol2cart(THETA,RHO);
RHO=ones(1,100)*50;
[X2,Y2] = pol2cart(THETA,RHO);
RHO=ones(1,100)*25;
[X3,Y3] = pol2cart(THETA,RHO);

hold on
plot(x,y,'-k')
plot(X,Y,'Color', [1 1 1]*.7);
plot(X1,Y1,'Color', [1 1 1]*.7);
plot(X2,Y2,'Color', [1 1 1]*.7);
plot(X3,Y3,'Color', [1 1 1]*.7);

axis equal
axis([-afstand*1.1 afstand*1.1 -afstand*1.1 afstand*1.1])
%hold off
saveas(f, 'plot.png');

